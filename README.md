# NX Look and Feel for Plasma 5 [![Build Status](https://travis-ci.org/nx-desktop/nx-plasma-look-and-feel.svg?branch=master)](https://travis-ci.org/nx-desktop/nx-plasma-look-and-feel)

This is the NX Look and Feel repository. Here you will find the following items:
- Plasma theme.
- Aurorae theme.
- SDDM Theme.
- Nomad color schemes.
- Konsole profile.
- Wallpapers.

# Requirements
- Plasma 5.8.4+.
- Global menu plasmoid.
- [Luv icon theme](https://gitlab.com/nitrux/nx-desktop/luv-icon-theme).
- [NX Systemtray](https://gitlab.com/nitrux/nx-desktop/nx-systemtray-applet)
- [NX notifications applet](https://gitlab.com/nitrux/nx-desktop/nx-notifications-applet)
- [NX networkmanagement applet](https://gitlab.com/nitrux/nx-desktop/nx-networkmanagement-applet).
- [NX audio applet](https://gitlab.com/nitrux/nx-desktop/nx-audio-applet).
- [NX clock applet](https://gitlab.com/nitrux/nx-desktop/nx-clock-applet).
- [Latte dock](https://phabricator.kde.org/source/latte-dock/).
- [NX Simplemenu](https://gitlab.com/nitrux/nx-desktop/x-simplemenu).

![](https://i.imgur.com/DnC6fwt.png)

# Installing
- Clone this Git repository or download zip.
- Extract Look and Feel package to `~/.local/share/plasma/look-and-feel/` or `/usr/share/plasma/look-and-feel/`.
- Extract Plasma theme to `~/.local/share/plasma/desktoptheme/` or `/usr/share/plasma/desktoptheme/`.
- Extract SDDM theme to `/usr/share/sddm/themes/`.
- Extract color schemes to `~/.local/share/color-schemes/` `/usr/share/color-schemes/`.
- Extract Konsole profile to `~/.local/share/konsole/` or `/usr/share/konsole/`.
- Extract wallpapers to `/usr/share/wallpapers/`.

# Issues
If you find problems with the contents of this repository please create an issue.

Artwork is Licensed under the [Creative Commons Attribution-Share Alike 4.0 License](https://creativecommons.org/licenses/by-sa/4.0/).

©2019 Nitrux Latinoamericana S.C.
